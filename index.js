window.addEventListener('DOMContentLoaded',()=>{
    const tiles = Array.from(document.querySelectorAll('.tile'));
    const playerDisplay = document.querySelector('.display-player');
    const resetButton = document.querySelector('#reset');
    const announcer = document.querySelector('.announcer');

    let board = ["", "", "", "", "", "", "", "", "",];
    let currentPlayer = 'X';
    let isGameActive = true;

    const PLAYERX_WON = 'PLAYERX_WON';
    const PLAYERO_WON = 'PLAYERO_WON';
    const TIE = 'TIE';


    /*
        com'è formata la grglia(per capire la win condition)
        [0] [1] [2]
        [3] [4] [5]
        [6] [7] [8]
    */

        const winningConditions = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];


        
        //funzione per la validazione di chi ha vinto
        function handleResultValidation(){
            let roundWon = false;
            for (let i = 0; i <= 7; i++) {
                const winCondition = winningConditions[i];
                const a = board[winCondition[0]];
                const b = board[winCondition[1]];
                const c = board[winCondition[2]];
                if (a === '' || b === '' || c === '') {
                    continue;
                }
                if (a === b && b === c) {
                    roundWon = true;
                    break;
                }
            }
            if (roundWon) {
                announce(currentPlayer === 'X' ? PLAYERX_WON : PLAYERO_WON);
                isGameActive = false;
                return;     
            }

            if (!board.includes(''))
            announce(TIE); 
        }





        //funzione dell'annunciatore che indica chi ha vinto o se si è in pareggio
        const announce = (type) =>{
            switch(type){
                case PLAYERO_WON:
                    announcer.innerHTML = 'Il giocatore <span class="payerO">O</span> Ha vinto';
                    break;
                case PLAYERX_WON:
                    announcer.innerHTML =   'Il giocatore <span class="payerX">X</span> Ha vinto';
                    break;
                case TIE:
                    announcer.innerHTML= 'Pareggio';
            }
            announcer.classList.remove('hide');
        };

        // funzione per assicurarsi che si possa giocare solo su caselle vuote

        const isValidAction = (tile) =>{
            if (tile.innerText === 'X' || tile.innerText === 'O'){
                return false;
            }

            return true;
        }

        // funzione che permette di inserire il simbolo del giocatore attuale
        const updateBoard = (index) =>{
            board[index] = currentPlayer;
        }

            // funzione per far cambiare il giocatore
        const changePlayer = () =>{
            playerDisplay.classList.remove(`player${currentPlayer}`);
            currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
            playerDisplay.innerText = currentPlayer;
            playerDisplay.classList.add(`player${currentPlayer}`);
        }

            //funzione per le azioni dei giocatori
        const userAction = (tile, index) => {
            if(isValidAction(tile) && isGameActive){
                tile.innerText = currentPlayer;
                tile.classList.add(`player${currentPlayer}`);
                updateBoard(index);
                handleResultValidation();
                changePlayer();
            }
        }
        tiles.forEach( (tile, index) => {
            tile.addEventListener('click', () => userAction(tile, index));
        });

        // funzione per resettare la tabella

        const resetBoard = () => {
            board = ["", "", "", "", "", "", "", "", "",];
            isGameActive = true;
            announcer.classList.add('hide');

            // il giocatore x sarà sempre il primo ad iniziare
            if (currentPlayer === 'O') {
                changePlayer();
            }
            // vengono svuotate le stringhe in modo da poter ricominciare
            tiles.forEach(tile => {
                tile.innerText = '';
                tile.classList.remove('playerX');
                tile.classList.remove('playerO');
            });
        }

    resetButton.addEventListener('click', resetBoard);
});